#include <dirent.h>
#include <errno.h>

#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#
 
#define D_BUFF 300

const int S_BUFF = sizeof(char) * D_BUFF; 
int fd_now = -1;
int id_now = -1;
 
const char *TABLE_OF_USERS = "./list_user_pass.csv"; // how to use : gcc server.c -pthread -o server && ./server
const char *PERM_TABLE = "/list_user_db.csv";
const char *LOG_FILE = "/home/nabil/Documents/FP/database/databases/db.log";
const char *dirNow = "/home/tegar/Documents/FP/database/databases"; // how to use : gcc client.c -pthread -o client && sudo ./client


const char *prepare = "/home/tegar/Documents/FP/database";

char thisDataB[1000];
char thisUser[1000];

void logging(char *nama, const char *command);
int create_tcp_server_socket();
void *prog(void *argv);
void *daemonize(pid_t *pid, pid_t *sid);

int main()
{
    pid_t pid, sid;
    daemonize(&pid, &sid);

    int check = mkdir(prepare, 0777);
    check = mkdir(dirNow,0777);

    socklen_t addrlen;
    struct sockaddr_in new_addr;
    pthread_t t_id;
    char dummy[D_BUFF];
    int server_fd = create_tcp_server_socket();
    int new_fd;
 
    while (1) {
        new_fd = accept(server_fd, (struct sockaddr *)&new_addr, &addrlen);
        if (new_fd >= 0) {
            printf("Terima koneksi database, fd: %d\n", new_fd);
            pthread_create(&t_id, NULL, &prog, (void *) &new_fd);
        } else {
            fprintf(stderr, "gagal [%s]\n", strerror(errno));
        }
    } 
    return 0;
}

void logging(char *nama, const char *command) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    FILE *fp = fopen(LOG_FILE, "a+");
    fprintf(fp, "%d-%02d-%02d %02d:%02d:%02d:%s:%s\n", timeinfo->tm_year + 1900,
            timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour,
            timeinfo->tm_min, timeinfo->tm_sec, nama, command);

    fclose(fp);
}

void logging(char *nama, const char *command) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    FILE *fp = fopen(LOG_FILE, "a+");
    fprintf(fp, "%d-%02d-%02d %02d:%02d:%02d:%s:%s\n", timeinfo->tm_year + 1900,
            timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour,
            timeinfo->tm_min, timeinfo->tm_sec, nama, command);

    fclose(fp);
}
 
void *prog(void *argv)
{
    // chdir(dirNow); 
    int fd = *(int *) argv;
    char query[D_BUFF], dummy[D_BUFF];
 
    while (read(fd, query, D_BUFF) != 0) {
        puts(query);
 
        strcpy(dummy, query);
        char *req = strtok(dummy, " ");
 
        if (strcmp(req, "LOGIN") == 0) {
            char *nama = strtok(NULL, " ");
            char *sandi = (strcmp(nama, "root") != 0) 
                            ? strtok(NULL, " ") : "root";
            strcpy(thisUser, nama);
        }
        else if (strcmp(req, "CREATE") == 0) {
            req = strtok(NULL, " ");
 
            if (strcmp(req, "USER") == 0) {
                if (id_now == 0) {
                    char *nama = strtok(NULL, " ");
                    
                    char *sandi = strtok(NULL, " ");
                    if (strcmp(sandi, "IDENTIFIED") == 0){
                        sandi = strtok(NULL, " ");
                        if (strcmp(sandi, "BY") == 0) {
                            sandi = strtok(NULL, " ");
                            // createAcc(fd, nama, sandi);        
                        }
                        else {
                            write(fd, "Wrong query\n\n", S_BUFF);
                        }
                    }
                    else {
                        write(fd, "Wrong query\n\n", S_BUFF);
                    }
                    
                    logging(thisUser, query);
                } else {
                    write(fd, "Cant access\n\n", S_BUFF);
                }
            }
            else if(strcmp(req, "DATABASE") == 0){
                req = strtok(NULL, " ");
                int check = mkdir(req, 0777);
                if (!check){
                    char directoryp[D_BUFF];
                    sprintf(directoryp, "%s/%s%s", dirNow, req, PERM_TABLE);
                    puts(directoryp);
                    FILE *baru = fopen(directoryp, "a");
                    fprintf(baru, "%d\n", id_now);
                    fclose(baru);
                    logging(thisUser, query);
                    write(fd, "Database created\n\n", S_BUFF);
                }
                else {
                    write(fd, "Unable to create database\n", S_BUFF);
                }

            }
            else {
                write(fd, "Wrong query\n\n", S_BUFF);
            }
        }
        else if(strcmp(req, "GRANT") == 0){
            req = strtok(NULL, " ");
            if(strcmp(req, "PERMISSION") == 0){
                if (id_now == 0) {
                    char *thisdb = strtok(NULL, " ");
                    char *into = strtok(NULL, " ");
                    char *thisuser = strtok(NULL, " ");
                    if (strcmp(into, "INTO") == 0) {
                        char directoryp[D_BUFF];
                        sprintf(directoryp, "%s/%s%s", dirNow, thisdb, PERM_TABLE);
                        puts(directoryp);
                        FILE *baru = fopen(directoryp, "a+");
                        int thisid = -1;
                        if (thisid != -1) {
                            fprintf(baru, "%d\n", thisid);
                            write(fd, "User Granted\n\n", S_BUFF);
                            logging(thisUser, query);
                        } else {
                            write(fd, "Wrong user\n\n", S_BUFF);
                        }
                        fclose(baru);
                    } else {
                        write(fd, "Wrong query\n\n", S_BUFF);
                    }
                } else {
                    write(fd, "Cant access\n\n", S_BUFF);
                }
            } else {
                write(fd, "Wrong query\n\n", S_BUFF);
            }
        } else if (strcmp(req, "USE") == 0) {
            req = strtok(NULL, " ");
            if (id_now == 0) {
                strcpy(thisDataB, req);
                puts(thisDataB);
                write(fd, "Database Granted\n\n", S_BUFF);
                logging(thisUser, query);
            } else {
                write(fd, "Wrong Database or Cant access\n\n",
                      S_BUFF);
            }
        } else {
            write(fd, "Wrong query\n\n", S_BUFF);
        }
    }
    if (fd == fd_now) {
        fd_now = id_now = -1;
    }
    printf("Close connection, fd: %d\n", fd);
    close(fd);
}

int create_tcp_server_socket()
{
    struct sockaddr_in saddr;
    int fd, ret_val;
    int opt = 1;
 
    fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
        fprintf(stderr, "socket failed [%s]\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    printf("Created a socket, fd: %d\n", fd);
 
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(7000);
    saddr.sin_addr.s_addr = INADDR_ANY;
 
    ret_val = bind(fd, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in));
    if (ret_val != 0) {
        fprintf(stderr, "bind failed [%s]\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
    }
 

    ret_val = listen(fd, 5);
    if (ret_val != 0) {
        fprintf(stderr, "listen failed [%s]\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
    }
    return fd;
}

void *daemonize(pid_t *pid, pid_t *sid)
{
    int status;
    *pid = fork();
 
    if (*pid != 0) {
        exit(EXIT_FAILURE);
    }
    if (*pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);
 
    *sid = setsid();
    if (*sid < 0 || chdir(dirNow) < 0) {
        exit(EXIT_FAILURE);
    }
 
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
